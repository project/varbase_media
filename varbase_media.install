<?php

/**
 * @file
 * Contains install and update for Varbase Media module.
 */

use Drupal\Core\File\FileSystemInterface;
use Vardot\Installer\ModuleInstallerFactory;
use Drupal\Core\File\FileExists;
use Drupal\Core\Recipe\Recipe;
use Drupal\Core\Recipe\RecipeRunner;

// Include all helpers and updates.
include_once __DIR__ . '/includes/helpers.inc';
include_once __DIR__ . '/includes/updates.inc';

/**
 * Implements hook_install().
 */
function varbase_media_install() {

  // Processor for install: in varbase_media.info.yml file.
  ModuleInstallerFactory::installList('varbase_media');

  ModuleInstallerFactory::importConfigsFromScanedDirectory('varbase_media', '/^field.storage.*\\.(yml)$/i');
  ModuleInstallerFactory::importConfigsFromScanedDirectory('varbase_media', '/^.*(settings.yml)$/i');
  ModuleInstallerFactory::importConfigsFromScanedDirectory('varbase_media', '/^entity_browser_enhanced.widgets.*\\.(yml)$/i');
  ModuleInstallerFactory::importConfigsFromScanedDirectory('varbase_media', '/^views.view.*\\.(yml)$/i');

  $module_path = Drupal::service('module_handler')->getModule('varbase_media')->getPath();

  // Load Media types icons for the media library table view page.
  $source = $module_path . '/images/icons';
  $destination = \Drupal::config('media.settings')->get('icon_base_uri');
  \Drupal::service('file_system')->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

  $files = \Drupal::service('file_system')->scanDirectory($source, '/.*\.(svg|png|jpg|jpeg|gif)$/');
  foreach ($files as $file) {
    if (!file_exists($destination . DIRECTORY_SEPARATOR . $file->filename)) {
      \Drupal::service('file_system')->copy($file->uri, $destination, FileExists::Error);
    }
  }

  // Import custom changes over media, media browser, and media library views.
  ModuleInstallerFactory::importConfigsFromScanedDirectory('varbase_media', '/^views.view.*\\.(yml)$/i');

  // Configure Entity Embed buttons.
  ModuleInstallerFactory::importConfigsFromScanedDirectory('varbase_media', '/^embed.button.*\\.(yml)$/i');

  $default_recipe = Recipe::createFromDirectory(__DIR__ . '/recipes/default');
  RecipeRunner::processRecipe($default_recipe);

}

/**
 * Implements hook_requirements().
 */
function varbase_media_requirements($phase) {
  $requirements = [];

  if ($phase == 'install') {
    $destination = \Drupal::config('media.settings')->get('icon_base_uri');
    \Drupal::service('file_system')->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $is_writable = is_writable($destination);
    $is_directory = is_dir($destination);
    if (!$is_writable || !$is_directory) {
      if (!$is_directory) {
        $error = t('The directory %directory does not exist.', ['%directory' => $destination]);
      }
      else {
        $error = t('The directory %directory is not writable.', ['%directory' => $destination]);
      }
      $description = t('An automated attempt to create this directory failed, possibly due to a permissions problem. To proceed with the installation, either create the directory and modify its permissions manually or ensure that the installer has the permissions to create it automatically. For more information, see INSTALL.txt or the <a href=":handbook_url">online handbook</a>.', [':handbook_url' => 'https://www.drupal.org/server-permissions']);
      if (!empty($error)) {
        $description = $error . ' ' . $description;
        $requirements['varbase_media']['description'] = $description;
        $requirements['varbase_media']['severity'] = REQUIREMENT_ERROR;
      }
    }
  }

  return $requirements;
}
